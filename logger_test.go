package logger

import (
	"io"
	"log"
	"os"
	"reflect"
	"testing"

	"github.com/getsentry/sentry-go"
)

// catchStdout re-routes stdout to separate pipe before function and switches it back after that, returns text catched in separate pipe
func catchStdout(t *testing.T, function func()) string {
	t.Helper()

	osStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	function()

	w.Close()
	out, err := io.ReadAll(r)
	if err != nil {
		t.Error(err)
	}
	os.Stdout = osStdout

	return string(out)
}

func TestNew(t *testing.T) {
	log := New("test", "example")

	if log.level != INFO {
		t.Fail()
	}
}

func TestGetHub(t *testing.T) {
	hub := &sentry.Hub{}
	log := New("test", "example", hub)

	if log.level != INFO {
		t.Fail()
	}
	if !reflect.DeepEqual(hub, log.GetHub()) {
		t.Fail()
	}
}

func TestGetLog(t *testing.T) {
	logger := New("", "INFO")
	stdLogger := log.New(os.Stdout, "", 0)

	if !reflect.DeepEqual(stdLogger, logger.GetLog()) {
		t.Fail()
	}
}

func TestGetLevel(t *testing.T) {
	text := "INFO"
	id := INFO
	logger := New("", text)

	if id != logger.level {
		t.Fail()
	}
	if text != logger.GetLevel() {
		t.Fail()
	}
}

func TestFatal(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Error("the code did not panic")
		}
	}()

	catchStdout(t, func() {
		logger := New("", "ERROR")
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
		logger.Fatal("Test")
	})
}

func TestError(t *testing.T) {
	expected := "ERROR Test\n"

	actual := catchStdout(t, func() {
		logger := New("", "ERROR")
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	if expected != actual {
		t.Fail()
	}
}

func TestError_Skip(t *testing.T) {
	expected := ""

	actual := catchStdout(t, func() {
		logger := New("", "ERROR")
		logger.Error("recovery(): Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	if expected != actual {
		t.Fail()
	}
}

func TestError_Level(t *testing.T) {
	expected := ""

	actual := catchStdout(t, func() {
		logger := New("", "FATAL")
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	if expected != actual {
		t.Fail()
	}
}

func TestWarn(t *testing.T) {
	expected := "ERROR Test\n"
	expected += "WARNING Test\n"

	actual := catchStdout(t, func() {
		logger := New("", "WARNING")
		logger.Error("Test")
		logger.Warnfln("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	if expected != actual {
		t.Fail()
	}
}

func TestInfo(t *testing.T) {
	expected := "ERROR Test\n"
	expected += "WARNING Test\n"
	expected += "INFO Test\n"

	actual := catchStdout(t, func() {
		logger := New("", "INFO")
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	if expected != actual {
		t.Fail()
	}
}

func TestDebug(t *testing.T) {
	expected := "ERROR Test\n"
	expected += "WARNING Test\n"
	expected += "INFO Test\n"
	expected += "DEBUG Test\n"

	actual := catchStdout(t, func() {
		logger := New("", "DEBUG")
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debugfln("Test")
		logger.Trace("Test")
	})

	if expected != actual {
		t.Fail()
	}
}

func TestTrace(t *testing.T) {
	expected := "ERROR Test\n"
	expected += "WARNING Test\n"
	expected += "INFO Test\n"
	expected += "DEBUG Test\n"
	expected += "TRACE Test\n"

	actual := catchStdout(t, func() {
		logger := New("", "TRACE")
		logger.Error("Test")
		logger.Warn("Test")
		logger.Info("Test")
		logger.Debug("Test")
		logger.Trace("Test")
	})

	if expected != actual {
		t.Fail()
	}
}
